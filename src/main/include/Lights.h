#pragma once

#include <frc/Spark.h>

class Lights {
public:
// http://www.revrobotics.com/content/docs/REV-11-1105-UM.pdf
	Lights(frc::Spark* spark);
	virtual ~Lights();

	enum Pattern
	{
		SOLIDRED = 0,
		STROBEBLUE,
		TWINKLESGREEN,
		COLORWAVESRAINBOW,
		SOLIDBLACK,
		SOLIDYELLOW,
		STROBERED,
		SOLIDBLUE,
		STROBEGOLD,

		PatternCount
	};

	void SetPattern(Pattern pattern);

private:

	static constexpr float patterns [PatternCount] =
	{
			0.61,
			-0.09,
			-0.47,
			-0.45,
			0.99,
			0.69,
			-0.11,
			0.87,
			-0.07
	};

	frc::Spark* m_spark;
};

