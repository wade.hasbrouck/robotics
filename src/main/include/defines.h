
// Joysticks
#define DRIVE_JOYSTICK          0
#define OPERATOR_JOYSTICK       1

// Joystick Buttons
#define ELEVATOR_TOP_BUTTON     3
#define ELEVATOR_MID_BUTTON     2
#define ELEVATOR_LOW_BUTTON     1
#define ELEVATOR_STOP_BUTTON    10
#define EXTENDER_EXTEND_BUTTON  4
#define EXTENDER_RETRACT_BUTTON 5

// Joystick 2 Buttons
#define AUTOPILOT_TOGGLE_BUTTON 1
#define GYRO_RESET_BUTTON       10

#define GRIPPER_OPEN_BUTTON     4
#define GRIPPER_CLOSE_BUTTON    5

// PWM Ports
#define DRIVE_MOTOR_LEFT_PWM    0
#define DRIVE_MOTOR_RIGHT_PWM   1

#define ELEVATOR_MOTOR_PWM      2
#define LIGHTS_PWM              3

// Relay ports
#define GRIPPER_RELAY           0
#define EXTENDER_RELAY          1

// Digital Input/Output ports
#define LINE_SENSOR_LEFT_DIO    0
#define LINE_SENSOR_CENTER_DIO  1
#define LINE_SENSOR_RIGHT_DIO   2

#define SONAR_DIO               3

#define EXTENDER_ENCODER_A_DIO  4
#define EXTENDER_ENCODER_B_DIO  5

#define GRIPPER_ENCODER_A_DIO   6
#define GRIPPER_ENCODER_B_DIO   7

#define EXTENDER_LIMITSWITCH_DIO    6

#define ELEVATOR_TOP_DIO        8
#define ELEVATOR_MIDDLE_DIO     9
#define ELEVATOR_BOTTOM_DIO     10

// Analog Inputs
#define GYRO_AI                 0
#define ACCELEROMETER_AI        1

// Accelerometer Settings
#define ACCELEROMETER_SENSITIVITY       0.3
#define ACCELEROMETER_ZERO              1.5

// Macros
#define THREAD_SLEEP_MILLISECONDS(x) std::this_thread::sleep_for(std::chrono::milliseconds(x))

// Gripper defines
#define GRIPPER_PULSE_LENGTH    500

// Extender Defines
#define EXTENDER_EXTEND_PULSE_LENGTH    2100
#define EXTENDER_RETRACT_PULSE_LENGTH   1600

// Elevator Defines
#define ELEVATOR_UP_SPEED               -1.00
#define ELEVATOR_HOLD_SPEED             -0.40
#define ELEVATOR_DOWN_SPEED             -0.10


