#pragma once

#include "LineSensor.h"
#include <frc/DigitalInput.h>

LineSensor::LineSensor(
    frc::DigitalInput* leftInput,
    frc::DigitalInput* centerInput,
    frc::DigitalInput* rightInput):
    m_leftInput(leftInput),
    m_centerInput(centerInput),
    m_rightInput(rightInput) {
}

void LineSensor::Update() {

    m_left = !m_leftInput->Get();
    m_center = !m_centerInput->Get();
    m_right = !m_rightInput->Get();

    m_leftAccumulator=0.5;
    m_rightAccumulator=0.5;

    if (m_left && !m_center && !m_right ) {
        m_leftAccumulator += 0.20;
    }
    else if (m_left && m_center && !m_right ) {
        m_leftAccumulator += 0.10;
    }
    else if(!m_left && m_center && !m_right ){

    }
    else if (!m_left && !m_center && m_right ){
        m_rightAccumulator +=0.20;
    }
    else if (!m_left && m_center && m_right ){
        m_rightAccumulator +=0.10;
    }
    else {
        m_leftAccumulator=0;
        m_rightAccumulator=0;
    }
}

bool LineSensor::FoundLine() {
    return m_left || m_center || m_right;
}

double LineSensor::GetLeftAccumulator() {
    return m_leftAccumulator;
}

double LineSensor::GetRightAccumulator() {
    return m_rightAccumulator;
}