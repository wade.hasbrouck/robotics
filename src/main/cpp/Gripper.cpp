#include <thread>
#include "Gripper.h"

Gripper::Gripper()
{
    this->m_pEncoder = NULL;
    this->m_pRelay = NULL;
    this->m_pThread = NULL;
}

Gripper::Gripper (frc::Relay *pRelay):
    Gripper::Gripper()
{
    this->m_pRelay = pRelay;
}

// Gripper::Gripper (frc::Relay *pRelay, frc::Encoder *pEncoder):
//     Gripper::Gripper(pRelay)
// {
//     this->m_pEncoder = pEncoder;
// }

void Gripper::Operate (Gripper::Action action, bool fUseEncoder)
{
    frc::Relay::Value direction = frc::Relay::Value::kOff;
    if (this->m_pThread)
    {
        this->m_ThreadStatus = Gripper::Status::RUNNING;
    }

    switch (action)
    {
        case Action::CLOSE :
            direction = frc::Relay::Value::kForward;
            break;
        case Action::OPEN :
            direction = frc::Relay::Value::kReverse;
            break;
        default:
            break;
    }

    this->m_pRelay->Set(direction);
    // if (fUseEncoder)
    // {
    //     while (this->m_pEncoder->GetRate() > 0 && !this->m_fStop)
    //     {
    //         THREAD_SLEEP_MILLISECONDS(5);
    //     }
    // }
    // else
    // {
        THREAD_SLEEP_MILLISECONDS(GRIPPER_PULSE_LENGTH);
    // }
    this->m_pRelay->Set(frc::Relay::Value::kOff);
    if (this->m_pThread)
    {
        this->m_ThreadStatus = Gripper::Status::STOPPED;
    }
}

void Gripper::Operate_thread (Gripper::Action action, bool fUseEncoder)
{
    if (this->m_pThread && this->m_ThreadStatus == Gripper::Status::RUNNING)
    {
        this->StopThread();
    }
    this->m_pThread = new std::thread (&Gripper::Operate, this, action, fUseEncoder);
}

Gripper::Status Gripper::GetThreadStatus()
{
    return this->m_ThreadStatus;
}

void Gripper::StopThread()
{
    if (this->m_pThread && this->m_ThreadStatus == Gripper::Status::RUNNING)
    {
        this->m_fStop = true;
        this->m_pThread->join();  // Wait for exit
    }
    if (this->m_pThread)
    {
        delete this->m_pThread;
        this->m_pThread = NULL;
    }
    this->m_ThreadStatus = Gripper::Status::STOPPED;
}

void Gripper::Open()
{

}

void Gripper::Close()
{

}