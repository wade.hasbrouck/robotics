#include <thread>
#include <iostream>
#include "Elevator.h"

Elevator::Elevator()
{
    this->m_pLowerLimitSwitch = NULL;
    this->m_pMidLimitSwitch = NULL;
    this->m_pUpperLimitSwitch = NULL;
    this->m_pMotor = NULL;

    this->m_fStop = false;
    this->m_pThread = NULL;
    this->m_pDestinationSwitch = NULL;
    this->m_threadStatus = Elevator::Status::STOPPED;

    this->m_CurrentPosition = Elevator::Position::BOTTOM;
}

Elevator::Elevator (frc::PWMSpeedController *pMotor,
                    frc::DigitalInput       *pLowSwitch,
                    frc::DigitalInput       *pMidSwitch,
                    frc::DigitalInput       *pHighSwitch):
    Elevator::Elevator()
{
    this->m_pMotor              = pMotor;        
    this->m_pLowerLimitSwitch   = pLowSwitch;
    this->m_pMidLimitSwitch     = pMidSwitch;
    this->m_pUpperLimitSwitch   = pHighSwitch;

    // See if we can determine location of the Elevator - Set to Unknown if unable to determine
    if (false == this->m_pLowerLimitSwitch->Get())
    {
        this->m_CurrentPosition = Position::BOTTOM;
    }
    else if (false == this->m_pMidLimitSwitch->Get())
    {
        this->m_CurrentPosition = Position::MIDDLE;
    }
    else if (false == this->m_pUpperLimitSwitch->Get())
    {
        this->m_CurrentPosition = Position::TOP;
    }
    else
    {
        this->m_CurrentPosition = Position::UNKNOWN;
    }
    if (this->m_fThreadRunning)
    {
        this->m_fThreadRunning = false;
    }
}

void Elevator::MoveTo_Thread(Position destination)
{
    std::cout << "Starting Elevator Thread..." << std::endl;
    if (this->m_pThread && this->m_threadStatus == Elevator::Status::RUNNING)
    {
        std::cout << "Stopping existing thread..." << std::endl;
        this->StopThread();
        std::cout << "Thread stopped." << std::endl;
    }
    this->m_pThread = new std::thread(&Elevator::MoveTo, this, destination);   
    std::cout << "Elevator thread started.  Exiting MoveTo_Thread()" << std::endl;
}

// TODO:  This needs some work around the case where moving from
// top to bottom and stopped and then directed to middle...
void Elevator::MoveTo (Position destination)
{
    std::cout << "Entering MoveTo()..." << std::endl;
    double speed = ELEVATOR_HOLD_SPEED;
   // frc::DigitalInput *pDestinationSwitch = NULL;

    if (this->m_pThread)
    {
        this->m_threadStatus == Elevator::Status::RUNNING;
    }

    switch (destination)
    {
        case Elevator::Position::BOTTOM :
            std::cout << "Destination is Bottom... " << std::endl;
            this->m_pDestinationSwitch = this->m_pLowerLimitSwitch;
            speed = ELEVATOR_DOWN_SPEED; 
            std::cout << "speed set to " << speed << std::endl;
            break;

        case Elevator::Position::MIDDLE :
            std::cout << "Destination is Middle..." << std::endl;
            this->m_pDestinationSwitch = this->m_pMidLimitSwitch;
            if (Elevator::Position::TOP == this->m_CurrentPosition)
            {
                std::cout << "At Top... setting speed to down" << std::endl;
                speed = ELEVATOR_DOWN_SPEED;
            }
            else if (Elevator::Position::BOTTOM == this->m_CurrentPosition)
            {
                std::cout << "At bottom... setting speed to up" << std::endl;
                speed = ELEVATOR_UP_SPEED;
            }
            // else  //we don't know which way to go... Hold!
            // {
            //     speed = ELEVATOR_HOLD_SPEED;
            // } 
            std::cout << "Speed set to " << std::endl;
            break;

        case Elevator::Position::TOP :
            std::cout << "Destination is Top..." << std::endl;
            this->m_pDestinationSwitch = this->m_pUpperLimitSwitch;
            speed = ELEVATOR_UP_SPEED;
            std::cout << "Speed set to " << speed << std::endl;
            break;

        default:
            break;
    }

    // Move the elevator until switch is closed.
    std::cout << "Starting motor... Speed is " << speed << std::endl;
    this->m_pMotor->Set(speed);
    while (true == this->m_pDestinationSwitch->Get())
    {
        if (this->m_fStop)
        {
            std::cout << "Elevator stopped by user or other code." << std::endl;
            break;
        }
        
        // Not sure if we need this...
        // if (false == this->m_pMidLimitSwitch->Get()) // Sense middle switch as we pass it and update current position
        // {
        //     this->m_CurrentPosition = Position::MIDDLE;
        // }

        THREAD_SLEEP_MILLISECONDS(5);
    }
    std::cout << "Reached desination... Setting hold speed." << std::endl;

    // Stop the Elevator - Hold speed
    this->m_pMotor->Set(ELEVATOR_HOLD_SPEED);

    if (!this->m_fStop) // Got to destination with out being stopped
    {
        std::cout << "Setting current position to destination..." << std::endl;
        this->m_CurrentPosition = destination;
    }
    else
    {
        this->m_fStop = false; // Stopped - rested
    }

    if (this->m_CurrentPosition == Elevator::Position::BOTTOM)
    {
        std::cout << "At the bottom... Turning the motor off..." << std::endl;
        this->m_pMotor->Set(0);  // At the bottom we can turn the motor off
    }

    if (this->m_pThread)
    {
        std::cout << "Setting thread status to Stopped" << std::endl;
        this->m_threadStatus = Elevator::Status::STOPPED;
    }
    this->m_pDestinationSwitch = NULL;
    std::cout << "Exiting Elevator::MoveTo()..." << std::endl;
}

void Elevator::StopThread()
{
    std::cout << "Entering Elevator::StopThread()" << std::endl;
    if (this->m_pThread && this->m_threadStatus == Elevator::Status::RUNNING)
    {
        std::cout << "Stopping and waiting..." << std::endl;
        this->m_fStop = true;
        this->m_pThread->join();  // Wait for exit
    }
    if (this->m_pThread)
    {
        std::cout << "deleting old thread object..." << std::endl;
        delete this->m_pThread;
        this->m_pThread = NULL;
    }
    this->m_threadStatus = Elevator::Status::STOPPED;
    std::cout << "Exiting Elevator::ThreadStop()..." << std::endl;   
}

void Elevator::GoUp()
{

}

void Elevator::GoDown()
{

}

void Elevator::Stop()
{
    // This function should stop or signal stop of the elevator travel.
    this->m_fStop = true;
}